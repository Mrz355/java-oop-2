package Mrz355;

import java.lang.String;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Marcin on 2016-04-09.
 */
public class Company {

    private static Company company = new Company( );
    private CEO ceo;

    private String name;
    /* A private Constructor prevents any other
     * class from instantiating.
     */
    private Company(){ }

    public static Company getInstance( ) {
        return company;
    }

    protected void hireCEO(CEO ceo) {
        this.ceo=ceo;
    }

    @Override
    public String toString() {
        if(ceo==null) return "Noone has been hired!";
        String companyStructure = ceo.name + " - CEO\n";
        StringBuilder res = new StringBuilder(companyStructure);

        for(Employee e: ceo.list ) {
            if (e.getClass() == BudgetOrientedManager.class || e.getClass() == CountOrientedManager.class) {
                res.append("\t" + e.name + " - Manager\n");
                for (Employee f : ((Manager) e).list) {
                    res.append("\t\t" + f.name + " - Employee\n");
                }
            } else {
                res.append("\t" + e.name + " - Employee\n");
            }
        }
        companyStructure=res.toString();
        return companyStructure;
    }

}
