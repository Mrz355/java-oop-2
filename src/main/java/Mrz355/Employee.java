package Mrz355;

/**
 * Created by Marcin on 2016-04-09.
 */
public class Employee {
    protected String name;
    protected long salary;

    public Employee(String name, long salary) {
        this.name=name;
        this.salary=salary;
    }
    public boolean isSatisfied() {
        return salary>10000;
    }


}
