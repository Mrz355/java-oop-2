package Mrz355;

/**
 * Created by Marcin on 2016-04-20.
 */
public class BudgetOrientedManager extends Manager {

    private long budget;

    public BudgetOrientedManager(String name, long salary, long budget) {
        super(name, salary);
        this.budget=budget;
    }

    @Override
    public boolean canHire(Employee employee) {
        int spentBudget=0;
        for (Employee e : list) {
            spentBudget += e.salary;
        }
        return budget-spentBudget>=employee.salary;
    }
}
