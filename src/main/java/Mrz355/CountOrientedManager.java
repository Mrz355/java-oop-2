package Mrz355;

/**
 * Created by Marcin on 2016-04-20.
 */
public class CountOrientedManager extends Manager {

    int count;

    public CountOrientedManager(String name, long salary, int count) {
        super(name,salary);
        this.count = count;
    }

    @Override
    public boolean canHire(Employee employee) {
        int numberOfEmployees=0;
        for (Employee e : list) {
            numberOfEmployees++;
        }
        return numberOfEmployees<count;
    }
}
