package Mrz355;

import java.util.ArrayList;

/**
 * Created by Marcin on 2016-04-09.
 */
// The only one hiring class :)
abstract public class Manager extends Employee {

    protected ArrayList<Employee> list;
    public Manager(String name, long salary) {
        super(name, salary);
        this.list = new ArrayList<Employee>();
    }


    public abstract boolean canHire(Employee employee);

    public void hire(Employee employee) {
        if(canHire(employee));
            list.add(employee);
    }


}
