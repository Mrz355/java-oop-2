package Mrz355;

import java.util.ArrayList;

/**
 * Created by Marcin on 2016-04-20.
 */
public class CEO extends Manager {

    public CEO(String name, long salary) {
        super(name, salary);
    }

    @Override
    public boolean canHire(Employee employee) {
        return true;    // CEO can hire everyone!
    }

}
