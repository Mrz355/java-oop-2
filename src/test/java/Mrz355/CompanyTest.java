package Mrz355;

import org.junit.Before;
import org.junit.Test;

import java.util.Vector;

import static org.junit.Assert.*;

/**
 * Created by Marcin on 2016-04-04.
 */
public class CompanyTest {

    Company company;
    CEO zbyszek;
    Employee employee1,employee2,employee3,employee4;
    static int count;
    Manager manager,manager1;

    @Before // to znaczy, że przed kazdym niezaleznym sceneriuszem inicjalizuje sie nowe helloWorld
    public void setUp() throws Exception {
        /*company = Company.getInstance();
        ceo = new CEO("Zbyszek",100000);
        company.hireCEO(ceo);
        employee1 = new Employee("Wacek",3000);
        employee2 = new Employee("Leszek",12000);
        employee3 = new Employee("Grzesiek",5000);
        employee4 = new Employee("Frodo",5000);

        manager = new CountOrientedManager("Maciek",10000,2);
        manager.hire(employee1);
        manager.hire(employee2);

        ceo.hire(employee3);

        manager1 = new BudgetOrientedManager("Krzysiek",14000,9000);
        manager1.hire(employee4);
        count++;*/
        company = Company.getInstance();
        zbyszek = new CEO("Zbyszek", 2000);
        company.hireCEO(zbyszek);
    }

    @Test
    public void shouldReturnCompanyHierarchy() throws Exception {
        BudgetOrientedManager maciek = new BudgetOrientedManager("Maciek", 2000, 20000);
        maciek.hire(new Employee("Wacek", 11000));
        maciek.hire(new Employee("Leszek", 3000));
        zbyszek.hire(maciek);
        zbyszek.hire(new Employee("Grzesiek", 1200));
        CountOrientedManager krzysiek = new CountOrientedManager("Krzysiek", 2100, 2);
        krzysiek.hire(new Employee("Frodo", 13000));
        zbyszek.hire(krzysiek);
        assertEquals("Zbyszek - CEO\n\tMaciek - Manager" +
                "\n\t\tWacek - Employee\n\t\tLeszek - Employee\n\tGrzesiek - Employee\n\t" +
                "Krzysiek - Manager\n\t\tFrodo - Employee\n", company.toString());
    }

}
