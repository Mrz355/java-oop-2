package Mrz355;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Marcin on 2016-04-20.
 */
public class CompanyMethodsTest {

    Company company;
    CEO ceo;
    Employee employee1,employee2,employee3,employee4;
    Manager manager,manager1;

    @Before
    public void setUp() throws Exception {
        company = Company.getInstance();
        ceo = new CEO("Zbyszek", 100000);
        company.hireCEO(ceo);
        employee1 = new Employee("Wacek", 3000);
        employee2 = new Employee("Leszek", 12000);
        employee3 = new Employee("Grzesiek", 5000);
        employee4 = new Employee("Frodo", 5000);

        manager = new CountOrientedManager("Maciek", 10000, 2);
        manager.hire(employee1);
        manager.hire(employee2);

        ceo.hire(employee3);

        manager1 = new BudgetOrientedManager("Krzysiek", 14000, 9000);
        manager1.hire(employee4);
    }

    @Test
    public void getInstance() throws Exception {
        company=company.getInstance();
    }

    @Test
    public void shouldReturnWhetherEmployeeIsSatisfyOrNot() throws Exception {
        assertEquals(false,employee1.isSatisfied());
        assertEquals(true,employee2.isSatisfied());
        assertEquals(false,employee3.isSatisfied());
        assertEquals(false,employee4.isSatisfied());
    }

    @Test
    public void shouldReturnWhetherCanHireOrNot() throws Exception {
        assertEquals(false,manager.canHire(employee3));
        assertEquals(false,manager.canHire(employee4));
        assertEquals(true,manager1.canHire(employee1));
        assertEquals(false,manager1.canHire(employee3));
    }
}
